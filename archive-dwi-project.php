<?php

/*
 * DWI Projects Archive
*/

get_header();

//Get Projects
$projects_args = array(
  'post_type' => 'dwi-project',
  'showposts' => -1,
  'order' => 'DESC',
  'post_status' => 'publish',
);
$projects = get_posts($projects_args);
?>

<main class="main">

  <section class="section section--title">
    <div class="container-fluid">

      <h1 class="title title--gold title--overhang">Recent Projects</h1>

      <div class="row">
        <?php if ($projects) : ?>
        <?php foreach ($projects as $project) : ?>
        <?php $proj_id = $project->ID ?>
        <div class="col-bp1-12 col-bp4-6">

          <div class="js-overlay project" style="background-image:url('<?php echo get_the_post_thumbnail_url($proj_id, 'large') ?>');">
            <div class="project__blend" style="background-image:url('<?php echo get_the_post_thumbnail_url($proj_id, 'large') ?>');"></div>
            <div class="project__title">
              <h4 class="title title--blue title--symbol">
                <a class="js-overlay-btn" href="<?php echo get_the_permalink($proj_id) ?>"><i class="title__symbol"></i><span><?php echo $project->post_title ?></span></a>
              </h4>
            </div>
            <div class="project__content-container">
              <div class="project__content">
                <div class="project__description">
                  <h5 class="project__subtitle">Project Address</h5>
                  <p><?php echo get_field('project_address', $proj_id) ?></p>
                  <h5 class="project__subtitle">Project GDV</h5>
                  <p><?php echo get_field('project_gdv', $proj_id) ?></p>
                  <h5 class="project__subtitle">Area</h5>
                  <p><?php echo get_field('project_area', $proj_id) ?></p>
                  <a href="<?php echo get_the_permalink($project->ID) ?>" class="button button--gold">View Project <span></span></a>
                </div>
              </div>
            </div>
          </div>

        </div>
        <?php endforeach ?>
        <?php endif ?>
      </div>

    </div>
  </section>

  <?php get_template_part('elements/components/cta-block'); ?>

</main>

<?php get_footer(); ?>