<div class="feature-bar">

  <?php echo do_shortcode('[instagram-feed id="5989902705" num=4 showheader=false showbutton=false showfollow=false imagepadding=5 imageres=full]'); ?>

  <div class="twitter">
    <header class="twitter__header">
      <h4 class="twitter__title">
        <a href="https://twitter.com/UrbanProvincial">
          <img src="<?php echo get_template_directory_uri() . '/assets/build/images/svgs/social-twitter.svg'; ?>" width="50" height="50" alt="Twitter bird" />
          <span>Twitter</span>
        </a>
      </h4>
      <a href="https://twitter.com/UrbanProvincial" class="button button--gold">Follow us <span></span></a>
    </header>
    <article class="twitter__description">
      <div class="tweet">
        <?php include(locate_template('lib/twitter.php')); ?>
      </div>
    </article>
  </div>

</div>