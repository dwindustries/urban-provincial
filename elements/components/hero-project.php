<?php if (have_rows('project_slider')) : ?>
<div class="hero hero--project">
  <div class="carousel hide-carousel">
    <div class="js-carousel-single swiper-container">
      <div class="swiper-wrapper carousel-items">

        <?php while (have_rows('project_slider')) : ?>
        <?php the_row() ?>
        <?php $img = get_sub_field('project_slider_image'); ?>
        <div class="swiper-slide">
          <div class="hero__slide hero__slide--project" style="background-image:url(<?php echo $img['url'] ?>)"></div>
        </div>
        <?php endwhile ?>

      </div>
    </div>

    <?php if (count(get_field('project_slider')) > 1) : ?>
    <footer class="carousel__footer carousel__footer--hero">
      <div class="container-fluid">
        <div class="row">
          <div class="col-bp1-12">
            <div class="carousel-nav">
              <div class="carousel-nav-prev">
                <i class="icon icon--arrow--left"></i>
              </div>
              <div class="carousel-nav-next">
                <i class="icon icon--arrow--right"></i>
              </div>
            </div>

            <div class="carousel-pager"></div>
          </div>
        </div>
      </div>
    </footer>
    <?php endif ?>
  </div>
</div>
<?php endif ?>