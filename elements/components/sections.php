<?php if (have_rows('sections')) : ?>
  <?php $i = 0; ?>
  <?php while (have_rows('sections')) : ?>
    <?php the_row() ?>
    <?php
        $section = get_sub_field('section');

        // Section Bg Colour
        $section_colour = '';
        $section_colour = strtolower($section['section_colour']);
        if ($section_colour === 'white') {
          $section_colour = 'block--reverse block--white block--text-dark';
        } else {
          $section_colour = 'block--blue block--text-light';
        }

        // Image Position left/right
        $section_position = '';
        if (strtolower($section['section_image_position']) === 'left')
          $section_position = 'block--reverse';

        // First block spacing
        $section_first = '';
        $section_about = '';
        $section_title = '';

        if ($i === 0) {
          $section_first = 'block--space-top';

          if (is_page_template('templates/about.php')) {
            $section_about = 'block--about'; // anomalous block style
          } else {
            $section_title = 'block__title';
          }
        } else {
          $section_title = 'block__title';
        }

        $block_content = '';
        if (is_page_template('templates/capabilities.php')) {
          $block_content = 'block__content--large';
        }

        // Collected section classes
        $section_classes = $section_colour . ' ' . $section_position . ' ' . $section_first . ' ' . $section_about;
        ?>

    <div class="block <?php echo $section_classes ?>">
      <?php if (strtolower($section['section_image_position']) === 'left') : ?>
        <div class="block__image">
          <img src="<?php echo $section['section_image']['sizes']['large']; ?>" alt="<?php echo $section['section_image']['alt'] ?>" />
          <?php if ($section['section_image_caption']) : ?>
            <h4 class="block__caption"><?php echo $section['section_image_caption'] ?></h4>
          <?php endif ?>
        </div>
      <?php endif ?>

      <div class="block__content <?php echo $block_content ?>">
        <h2 class="<?php echo $section_title; ?>"><?php echo $section['section_title'] ?></h2>
        <?php echo $section['section_description'] ?>
      </div>

      <?php if (strtolower($section['section_image_position']) === 'right') : ?>
        <div class="block__image">
          <img src="<?php echo $section['section_image']['sizes']['large']; ?>" alt="<?php echo $section['section_image']['alt'] ?>" />
          <?php if ($section['section_image_caption']) : ?>
            <h4 class="block__caption"><?php echo $section['section_image_caption'] ?></h4>
          <?php endif ?>
        </div>
      <?php endif ?>
    </div>

    <?php $i++ ?>
  <?php endwhile ?>
<?php endif ?>