<?php

// Pagination links
if (is_single()) {
  $next_link = get_adjacent_post(true, '', false);
  $prev_link = get_adjacent_post(true, '', true);
}
?>

<?php if ($prev_link || $next_link) : ?>

<?php if (is_single()) : ?>

<div class="pagination">
  <?php if ($prev_link && $next_link) : ?>
  <div class="pagination__item">
    <a class="button button--gold button--reverse" href="<?php echo $prev_link->guid ?>">Previous<span></span></a>
  </div>
  <div class="pagination__item">
    <a class="button button--gold" href="<?php echo $next_link->guid ?>">Next<span></span></a>
  </div>
  <?php elseif (!$next_link && $prev_link) : ?>
  <div class="pagination__item">
    <a class="button button--gold button--reverse" href="<?php echo $prev_link->guid ?>">Previous<span></span></a>
  </div>
  <?php elseif (!$prev_link && $next_link) : ?>
  <div class="pagination__item">
    <a class="button button--gold" href="<?php echo $next_link->guid ?>">Next<span></span></a>
  </div>
  <?php endif ?>
</div>

<?php endif ?>

<?php endif ?>