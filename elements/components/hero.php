<?php if (have_rows('home_slider')) : ?>
  <section class="section section--space">
    <div class="hero">
      <div class="carousel hide-carousel">
        <div class="js-carousel-single swiper-container">
          <div class="swiper-wrapper carousel-items">

            <?php while (have_rows('home_slider')) : ?>
              <?php the_row() ?>
              <?php $slide = get_sub_field('home_slide'); ?>

              <div class="swiper-slide">
                <div class="hero__slide hero__slide--home" style="background-image:url(<?php echo $slide['home_slide_image']['url']; ?>)">
                  <div class="container-fluid">
                    <div class="row">
                      <div class="col-bp1-12">

                        <div class="hero__content">
                          <h1 class="title title--white title--fancy title--caps"><?php echo $slide['home_slide_title']; ?></h1>
                          <h2 class="title title--small title--gold"><?php echo $slide['home_slide_sub_title']; ?></h2>
                          <div style="clear:both"></div>
                          <a href="<?php echo $slide['home_slide_link_url']; ?>" class="button button--white">Project: <em><?php echo $slide['home_slide_link_text']; ?></em><span></span></a>
                        </div>

                      </div>
                    </div>
                  </div>
                </div>
              </div>

            <?php endwhile ?>

          </div>
        </div>

        <footer class="carousel__footer carousel__footer--hero">
          <div class="carousel-nav">
            <div class="carousel-nav-prev">
              <i class="icon icon--arrow--left"></i>
            </div>
            <div class="carousel-nav-next">
              <i class="icon icon--arrow--right"></i>
            </div>
          </div>

          <div class="carousel-pager"></div>
        </footer>
      </div>
    </div>
  </section>
<?php endif ?>