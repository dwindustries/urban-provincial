<?php $contact_page_id = 23; ?>

<div class="cta-block">
  <a href="contact" class="button button--gold button--large">Contact us <span></span></a>
  <div class="cta-block__content">
    <h4>Let’s talk</h4>
    <p>Call us on <?php echo get_field('contact_telephone', $contact_page_id); ?> or email us at <a href="mailto:<?php echo get_field('contact_email', $contact_page_id); ?>"><?php echo get_field('contact_email', $contact_page_id); ?></a></p>
  </div>
</div>