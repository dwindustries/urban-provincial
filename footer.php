<?php $contact_page_id = 23; ?>

<footer class="site-footer">
  <div class="container-fluid">
    <div class="row">
      <div class="col-bp1-12">

        <div class="site-footer__inner">
          <img class="site-footer__brand" src="<?php echo get_stylesheet_directory_uri() . '/assets/images/logos/urban-and-provincial-logo.svg'; ?>" alt="Urban &amp; Provincial" />

          <div class="site-footer__desc">
            <p>&copy; Copyright Urban &amp; Provincial Ltd <br /><?php echo get_field('contact_address', $contact_page_id); ?></p>
            <p></p>
          </div>

          <div class="site-footer__details">
            <ul>
              <li><a href="tel:<?php echo get_field('contact_telephone', $contact_page_id); ?>"><?php echo get_field('contact_telephone', $contact_page_id); ?></a></li>
              <li><a href="mailto:<?php echo get_field('contact_email', $contact_page_id); ?>"><?php echo get_field('contact_email', $contact_page_id); ?></a>
            </ul>

            <ul>
              <li><a href="/privacy">Privacy</li>
              <li><a href="/">Home page</a>
              <li><a href="#top">Return to top</a>
            </ul>
          </div>

          <div class="site-footer__creator">
            <p>Site design by <a href="http://broadgatecreative.co.uk" target="blank">Broadgate Creative</a></p>
          </div>
        </div>

      </div>
    </div>
  </div>
</footer>

</div>
</div>


<?php wp_footer(); ?>

</body>

</html>