<?php

/*
  *  DWI Project Single
  */

get_header();
?>

<main class="main">

  <section class="section section--single-project">
    <div class="container-fluid">
      <div class="row">
        <div class="col-bp1-12">

          <div class="single-project">
            <div class="single-project__header">

              <h1 class="title title--gold title--caps" style="z-index:2;"><?php echo the_title() ?></h1>
              <h2 class="title title--blue title--small" style="z-index:2;"><?php echo get_field('project_sub_heading') ?></h2>

              <div class="single-project__post">
                <h3>Project Address</h3>
                <p><?php echo get_field('project_address') ?></p>

                <h3>Project GDV</h3>
                <p><?php echo get_field('project_gdv') ?></p>

                <h3>Area</h3>
                <p><?php echo get_field('project_area') ?></p>

                <h3>Timescale</h3>
                <p><?php echo get_field('project_timescale') ?></p>

                <h3>Key Clients</h3>
                <p><?php echo get_field('project_key_clients') ?></p>
              </div>
            </div>

            <?php get_template_part('elements/components/hero-project'); ?>
          </div>

        </div>
      </div>

    </div>
  </section>

  <section class="section section--space">
    <div class="container-fluid">
      <div class="row">

        <div class="col-bp1-12 col-bp3-7">

          <?php
          $coords = get_field('project_map_coords');
          $coords_lat = $coords['project_map_coords_lat'];
          $coords_long = $coords['project_map_coords_long'];
          ?>
          <?php if ($coords) : ?>
          <div id="js-map-2d" class="map" data-lat="<?php echo $coords_lat ?>" data-long="<?php echo $coords_long ?>"></div>
          <?php endif ?>

        </div>
        <div class="col-bp1-12 col-bp3-5">
          <?php if ($coords) : ?>
          <div id="js-map-3d" class="map map--compact" data-lat="<?php echo $coords_lat ?>" data-long="<?php echo $coords_long ?>"></div>
          <?php endif ?>
        </div>

      </div>
    </div>
  </section>

  <section class="section section--space">
    <div class="container-fluid">
      <div class="row">

        <div class="col-bp1-12 col-bp3-7">
          <article class="single-project__description">
            <?php echo get_field('project_description') ?>
          </article>
        </div>
        <div class="col-bp1-12 col-bp3-5">
          <?php $img = get_field('project_description_image'); ?>
          <?php if ($img) : ?>
          <img class="single-project__image" src="<?php echo $img['sizes']['large'] ?>" alt="<?php echo $img['alt'] ?>" />
          <?php endif ?>
        </div>

      </div>
    </div>
  </section>

  <?php get_template_part('elements/components/cta-block'); ?>

</main>

<?php get_footer(); ?>