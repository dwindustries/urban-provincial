<?php

# Load Twitter class
require_once('TwitterOAuth.php');

# Define constants
define('TWEET_LIMIT', 1);
define('TWITTER_USERNAME', 'UrbanProvincial');
define('CONSUMER_KEY', '0BlkSYO7WRc4nLBy6wSGA');
define('CONSUMER_SECRET', 'g17o85cdfpo2OKhPzb6SJd15ejt1hpntbluIoDZT0');
define('ACCESS_TOKEN', '1222979774-1D9qomWaNNTCYHNqQkakdL2cqedyPemc1EVtFO0');
define('ACCESS_TOKEN_SECRET', 'h6Yt8xka0DySND83mbXbn0kOHJxZISij69AhvlRg');

# Create the connection
$twitter = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, ACCESS_TOKEN, ACCESS_TOKEN_SECRET);

# Migrate over to SSL/TLS
$twitter->ssl_verifypeer = true;

# Load the Tweets
$tweets = $twitter->get('statuses/user_timeline', array('screen_name' => TWITTER_USERNAME, 'exclude_replies' => 'true', 'include_rts' => 'false', 'count' => TWEET_LIMIT));

# Example output
if (!empty($tweets)) {
  foreach ($tweets as $tweet) {

    # Access as an object
    $tweetText = $tweet['text'];

    # Make links active http and https
    $tweetText = preg_replace("#(http://|(www\.))(([^\s<]{4,68})[^\s<]*)#", '<a href="http://$2$3" target="_blank">$1$2$4</a>', $tweetText);
    $tweetText = preg_replace("#(https://|(www\.))(([^\s<]{4,68})[^\s<]*)#", '<a href="https://$2$3" target="_blank">$1$2$4</a>', $tweetText);

    # Linkify user mentions
    $tweetText = preg_replace("/@(\w+)/i", '<a href="http://twitter.com/$1" target="_blank">$0</a>', $tweetText);

    # Linkify hashtags
    if (true === array_key_exists('hashtags', $tweet['entities'])) {
      $linkified = array();
      foreach ($tweet['entities']['hashtags'] as $hashtag) {
        $hash = $hashtag['text'];
        if (in_array($hash, $linkified)) {
          continue; // do not process same hash twice or more
        }
        $linkified[] = $hash;
        // replace single words only, so looking for #Google we wont linkify >#Google<Reader
        $tweetText = preg_replace('/#\b' . $hash . '\b/', sprintf('<a href="https://twitter.com/search?q=%%23%2$s&src=hash" target="_blank">#%1$s</a>', $hash, urlencode($hash)), $tweetText);
      }
    }

    // Media
    if (true === array_key_exists('media', $tweet['entities'])) {
      $linkified = array();
      foreach ($tweet['entities']['media'] as $media) {
        $mediaUrl = $media['media_url'];
        $media = $media['url'];
        if (in_array($media, $linkified)) {
          continue; // do not process same url twice or more
        }
        $linkified[] = $media;
        $text = str_replace($media, sprintf('<a href="%1$s" title="%2$s">%1$s</a>', $media, $mediaUrl), $text);
      }
    }

    # Output the tweet
    echo '<p>' . $tweetText . '</p>';
  }
}
