<?php

/**
 * The Template for displaying all single posts.
 *
 * @package     WordPress
 * @subpackage  Urban Provincial
 * @since       Urban Provincial 1.0
 */

get_header();

?>

<main class="main">

  <section class="section section--news section--space">
    <div class="container-fluid">

      <h1 class="title title--gold title--news"><?php echo get_the_title(get_option('page_for_posts', true)) ?></h1>

      <?php if (have_posts()) : ?>
      <?php while (have_posts()) : ?>
      <?php the_post(); ?>
      <div class="row">
        <div class="col-bp1-12">
          <header class="single-post__header">
            <p class="single-post__date"><?php echo get_the_date('F Y') ?></p>
          </header>
        </div>
      </div>

      <div class="row">
        <div class="col-bp1-12 col-bp5-7">

          <article class="single-post__article">
            <h2><?php the_title() ?></h2>
            <?php the_content() ?>
          </article>

        </div>
        <div class="col-bp1-12 col-bp5-5">
          <?php $img1 = get_field('post_image_1'); ?>
          <?php if (!empty($img1)) : ?>
          <img class="single-post__image" src="<?php echo $img1['sizes']['large'] ?>" alt="<?php echo $img1['alt'] ?>" />
          <?php endif ?>

          <?php $img2 = get_field('post_image_2'); ?>
          <?php if (!empty($img2)) : ?>
          <img class="single-post__image" src="<?php echo $img2['sizes']['large'] ?>" alt="<?php echo $img2['alt'] ?>" />
          <?php endif ?>
        </div>

        <?php endwhile ?>
        <?php endif ?>
      </div>

    </div>
  </section>

  <?php get_template_part('elements/components/pagination'); ?>
  <?php get_template_part('elements/components/cta-block'); ?>

  <section class="section section--space">
    <div class="container-fluid">
      <div class="row">
        <div class="col-bp1-12">

          <?php get_template_part('elements/components/feature-bar'); ?>

        </div>
      </div>
    </div>
  </section>
</main> <?php get_footer(); ?>