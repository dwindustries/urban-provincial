<?php

/**
 * Functions and definitions
 *
 * @package     WordPress
 * @subpackage  Urban Provincial
 * @since       Urban Provincial 1.0
 */

/* Thumbnail support */
add_theme_support('post-thumbnails');

// Add new image sizes
if (function_exists('add_image_size')) {
  add_image_size('article-list', 400, 300, true); // 220 pixels wide by 180 pixels tall, hard crop mode
}

// Create the Custom Primary Navigation
require_once 'functions/tidy-wp-admin.php';


// Register Nav Menu
function register_menu()
{
  register_nav_menu('primary-menu', __('Primary Menu'));
}
add_action('init', 'register_menu');


// Custom Excerpt
function custom_excerpt_length()
{
  return 29;
}
add_filter('excerpt_length', 'custom_excerpt_length', 999);

function new_excerpt_more($more)
{
  return '&hellip;';
}
add_filter('excerpt_more', 'new_excerpt_more');


/* ======================*
   *  Scripts & Styles     *
	 * ======================*/

// Change Stylesheet Directory
function stylesheet_dir()
{

  wp_register_style('all', get_stylesheet_directory_uri() . '/assets/build/css/all.css');
  wp_enqueue_style('all');
}
add_action('wp_enqueue_scripts', 'stylesheet_dir');

/* Add scripts via wp_head() */
function script_enqueuer()
{
  wp_register_script('global-js', get_template_directory_uri() . '/assets/build/js/global.js', '', '1.0', true);
  wp_enqueue_script('global-js');

  // Localize the ajax url for use in js
  // What page are we on? And what is the pages limit?
  global $wp_query;
  $max = $wp_query->max_num_pages;
  $paged = (get_query_var('paged') > 1) ? get_query_var('paged') : 1;

  // Add parameters for the AJAX js.
  wp_localize_script(
    'global-js',
    'ajaxpagination',
    array(
      'startPage' => $paged,
      'maxPages' => $max,
      'nextLink' => next_posts($max, false)
    )
  );
}
add_action('wp_enqueue_scripts', 'script_enqueuer');
