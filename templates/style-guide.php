<?php

/*
 * Template Name: Style Guide
*/

get_header();
?>

<main class="main">

  <section class="section section--padding-top-bottom">
    <div class="container-fluid">
      <div class="row">
        <div class="col-bp1-12">

          <h1 style="font-size:3.4em; margin-bottom:.2em;">Style Guide</h1>
          <hr />
          <h2 style="font-size:2.8em;">Type Styles</h2>
          <h1>h1 Aenean commodo ligula eget dolor aenean massa</h1>
          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa
            <strong>strong</strong>. Cum sociis natoque penatibus
            et magnis dis parturient montes, nascetur ridiculus
            mus. Donec quam felis, ultricies nec, pellentesque
            eu, pretium quis, sem. Nulla consequat massa quis
            enim. Donec pede justo, fringilla vel, aliquet nec,
            vulputate eget, arcu. In enim justo, rhoncus ut,
            imperdiet a, venenatis vitae, justo. Nullam dictum
            felis eu pede <a href="#">link example</a> dictam enim.
          </p>

          <h2>h2 Aenean commodo ligula eget dolor aenean massa</h2>
          <p>
            mollis pretium. Integer tincidunt. Cras dapibus.
            Vivamus elementum semper nisi. Aenean vulputate
            eleifend tellus. Aenean leo ligula, porttitor eu,
            consequat vitae, eleifend ac, enim. Aliquam lorem ante,
            dapibus in, viverra quis, feugiat a, tellus. Phasellus
            viverra nulla ut metus varius laoreet. Quisque rutrum.
            Aenean imperdiet. Etiam ultricies nisi vel augue.
            Curabitur ullamcorper ultricies nisi.</p>

          <h3>h3 Aenean commodo ligula eget dolor aenean massa</h3>
          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing
            elit. Aenean commodo ligula eget dolor. Aenean massa.
            Cum sociis natoque penatibus et magnis dis parturient
            montes, nascetur ridiculus mus. Donec quam felis,
            ultricies nec, pellentesque eu, pretium quis, sem.</p>

          <h4>h4 Aenean commodo ligula eget dolor aenean massa</h4>
          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing
            elit. Aenean commodo ligula eget dolor. Aenean massa.
            Cum sociis natoque penatibus et magnis dis parturient
            montes, nascetur ridiculus mus. Donec quam felis,
            ultricies nec, pellentesque eu, pretium quis, sem.</p>

          <h5>h5 Aenean commodo ligula eget dolor aenean massa</h5>
          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing
            elit. Aenean commodo ligula eget dolor. Aenean massa.
            Cum sociis natoque penatibus et magnis dis parturient
            montes, nascetur ridiculus mus. Donec quam felis,
            ultricies nec, pellentesque eu, pretium quis, sem.</p>

          <h6>h6 Aenean commodo ligula eget dolor aenean massa</h6>
          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing
            elit. Aenean commodo ligula eget dolor. Aenean massa.
            Cum sociis natoque penatibus et magnis dis parturient
            montes, nascetur ridiculus mus. Donec quam felis,
            ultricies nec, pellentesque eu, pretium quis, sem.</p>
          <ul>
            <li>Lorem ipsum dolor sit amet consectetuer.</li>
            <li>Aenean commodo ligula eget dolor.</li>
            <li>Aenean massa cum sociis natoque penatibus.</li>
          </ul>
          <ol>
            <li>Lorem ipsum dolor sit amet consectetuer.</li>
            <li>Aenean commodo ligula eget dolor.</li>
            <li>Aenean massa cum sociis natoque penatibus.</li>
          </ol>
          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing
            elit. Aenean commodo ligula eget dolor. Aenean massa.
            Cum sociis natoque penatibus et magnis dis parturient
            montes, nascetur ridiculus mus. Donec quam felis,
            ultricies nec, pellentesque eu, pretium quis, sem.</p>

          <table>
            <thead>
              <tr>
                <th>Title 1</th>
                <th>Title 2</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td data-th="Title 1">
                  <p>Lorem ipsum dolor sit amet, consectetuer adipiscing
                    elit. Aenean commodo ligula eget dolor.</p>
                </td>
                <td data-th="Title 2">
                  <p>Lorem ipsum dolor sit amet, consectetuer adipiscing
                    elit. Aenean commodo ligula eget dolor. Aenean massa.
                    Cum sociis natoque penatibus et magnis dis parturient
                    montes, nascetur ridiculus mus. Donec quam felis,
                    ultricies nec, pellentesque eu, pretium quis, sem.</p>
                </td>
              </tr>
              <tr>
                <td data-th="Title 1">
                  <p>Lorem ipsum dolor sit amet, consectetuer adipiscing
                    elit. Aenean commodo ligula eget dolor. </p>
                </td>
                <td data-th="Title 2">
                  <p>Lorem ipsum dolor sit amet, consectetuer adipiscing
                    elit. Aenean commodo ligula eget dolor. Aenean massa.
                    Cum sociis natoque penatibus et magnis dis parturient
                    montes, nascetur ridiculus mus. Donec quam felis,
                    ultricies nec, pellentesque eu, pretium quis, sem.</p>
                </td>
              </tr>
            </tbody>
          </table>

          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing
            elit. Aenean commodo ligula eget dolor. Aenean massa.
            Cum sociis natoque penatibus et magnis dis parturient
            montes, nascetur ridiculus mus. Donec quam felis,
            ultricies nec, pellentesque eu, pretium quis, sem.</p>

          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing
            elit. Aenean commodo ligula eget dolor. Aenean massa.
            Cum sociis natoque penatibus et magnis dis parturient
            montes, nascetur ridiculus mus. Donec quam felis,
            ultricies nec, pellentesque eu, pretium quis, sem.</p>
          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing
            elit. Aenean commodo ligula eget dolor. Aenean massa.
            Cum sociis natoque penatibus et magnis dis parturient
            montes, nascetur ridiculus mus. Donec quam felis,
            ultricies nec, pellentesque eu, pretium quis, sem.</p>

        </div>
      </div>

      <div class="row">
        <div class="col-bp1-12">

          <p>&nbsp;</p>
          <h1>Titles</h1>
          <h1 class="title title--blue title--caps">Ruby Triangle</h1>
          <h1 class="title title--gold">News</h1>
          <h1 class="title title--gold title--caps">ROTHERHITHE NEW ROAD</h1>
          <h2 class="title title--gold title--small">Luxury Residences</h2>

        </div>
      </div>
    </div>
  </section>

  <section class="hero">
    <div class="carousel hide-carousel">
      <div class="js-carousel-single swiper-container">
        <div class="swiper-wrapper carousel-items">

          <div class="swiper-slide">
            <div class="hero__slide" style="background-image:url(<?php echo site_url() . '/wp-content/uploads/2019/08/hero-placeholder.jpg'; ?>)">
              <div class="container-fluid">
                <div class="row">
                  <div class="col-bp1-12">

                    <div class="hero__content">
                      <h1 class="title title--white title--fancy title--caps">Masterpieces...</h1>
                      <h2 class="title title--small title--gold">Luxury Residences</h2>
                      <div style="clear:both"></div>
                      <a class="button button--white">Projects: <em>Churchills</em> <span></span></a>
                    </div>

                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="swiper-slide">
            <div class="hero__slide" style="background-image:url(<?php echo site_url() . '/wp-content/uploads/2019/08/hero-placeholder.jpg'; ?>)">
              <div class="container-fluid">
                <div class="row">
                  <div class="col-bp1-12">

                    <div class="hero__content">
                      <h1 class="title title--white title--fancy title--caps">Masterpieces...</h1>
                      <h2 class="title title--small title--gold">Luxury Residences</h2>
                      <div style="clear:both"></div>
                      <a class="button button--white">Projects: <em>Churchills</em> <span></span></a>
                    </div>

                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>

      <footer class="carousel__footer carousel__footer--hero">
        <div class="container-fluid">
          <div class="row">
            <div class="col-bp1-12">
              <div class="carousel-nav">
                <div class="carousel-nav-prev">
                  <i class="icon icon--arrow--left"></i>
                </div>
                <div class="carousel-nav-next">
                  <i class="icon icon--arrow--right"></i>
                </div>
              </div>

              <div class="carousel-pager"></div>
            </div>
          </div>
        </div>
      </footer>
    </div>
  </section>


  <section class="section section--padding-top-bottom">
    <div class="container-fluid">
      <div class="row">
        <div class="col-bp1-12 col-bp4-4">
          <h1>Modules</h1>
        </div>
      </div>

      <div class="row">
        <div class="col-bp1-12 col-bp4-6">

          <section class="section section--space section--padding-around section--blue">
            <form action="#" method="post">
              <fieldset>
                <div class="form__group">
                  <label class="form__label" for="name">Name</label>
                  <input class="form__input form__input--text" type="text" id="name" placeholder="Enter your full name" />
                </div>
                <div class="form__group">
                  <label class="form__label" for="email">Email</label>
                  <input class="form__input form__input--text" type="email" id="email" placeholder="Enter your email address" />
                </div>
                <div class="form__group">
                  <label class="form__label" for="message">Message</label>
                  <textarea class="form__input form__input--textarea" id="message" placeholder="What's on your mind?"></textarea>
                </div>
                <button class="button button--gold" type="submit">Send Form <span></span></button>
              </fieldset>
            </form>
          </section>

        </div>
      </div>

      <div class="row">
        <div class="col-bp1-12 col-bp2-6 col-bp4-4">

          <div class="js-overlay feature" style="background-image:url('<?php echo site_url() . '/wp-content/uploads/2019/07/feature-placeholder1.jpg'; ?>);">
            <a href="#" class="js-overlay-btn feature__trigger" title="View Feature Description">
              <span></span>
            </a>
            <div class="feature__content-container">
              <div class="feature__content">
                <h4 class="feature__title">Projects:</h4>
                <p>Rotherhithe <br />New Road</p>
                <a href="/" class="button button--gold">View Project <span></span></a>
              </div>
            </div>
          </div>

          <p>&nbsp;</p>

        </div>
      </div>

      <div class="row">
        <div class="col-bp1-12 col-bp2-6 col-bp5-6">

          <div class="twitter">
            <header class="twitter__header">
              <h4 class="twitter__title">
                <a href="">
                  <img src="<?php echo get_template_directory_uri() . '/assets/build/images/svgs/social-twitter.svg'; ?>" width="50" height="50" alt="Twitter bird" />
                  <span>Twitter</span>
                </a>
              </h4>
              <a href="#" class="button button--gold">Follow us <span></span></a>
            </header>
            <article class="twitter__description">
              <p>According to all known laws of aviation, there is no way that a bee should be able to fly. Its wings are too small to get its fat little body off the ground. The bee, of course, flies anyways. Because bees don't care what humans think is impossible. Yellow, black. <a href="#">#280characters</a></p>
            </article>
          </div>

          <p>&nbsp;</p>

        </div>
      </div>

    </div>
  </section>


  <section class="section">
    <div class="container-fluid">

      <div class="row">
        <div class="col-bp1-12 col-bp4-6">

          <div class="js-overlay project" style="background-image:url('<?php echo site_url() . '/wp-content/uploads/2019/07/feature-placeholder1.jpg'; ?>);">
            <div class="project__title">
              <h4 class="title title--blue title--symbol">
                <a class="js-overlay-btn" href="#"><i class="title__symbol"></i><span>Ruby Triangle</span></a>
              </h4>
            </div>
            <div class="project__content-container">
              <div class="project__content">
                <div class="project__description">
                  <h5 class="project__subtitle">Project Address</h5>
                  <p>Rotherhithe New Road, London, SE16 3DH</p>
                  <h5 class="project__subtitle">Project GDV</h5>
                  <p>Circa £75 million</p>
                  <h5 class="project__subtitle">Area</h5>
                  <p>Mixed use development comprising educational and residential</p>
                  <a href="/" class="button button--gold">View Project <span></span></a>
                </div>
              </div>
            </div>
          </div>

        </div>
        <div class="col-bp1-12 col-bp4-6">

          <div class="js-overlay project" style="background-image:url('<?php echo site_url() . '/wp-content/uploads/2019/07/feature-placeholder1.jpg'; ?>);">
            <div class="project__title">
              <h4 class="title title--blue title--symbol">
                <a class="js-overlay-btn" href="#"><i class="title__symbol"></i><span>Ruby Triangle</span></a>
              </h4>
            </div>
            <div class="project__content-container">
              <div class="project__content">
                <div class="project__description">
                  <h5 class="project__subtitle">Project Address</h5>
                  <p>Rotherhithe New Road, London, SE16 3DH</p>
                  <h5 class="project__subtitle">Project GDV</h5>
                  <p>Circa £75 million</p>
                  <h5 class="project__subtitle">Area</h5>
                  <p>Mixed use development comprising educational and residential</p>
                  <a href="/" class="button button--gold">View Project <span></span></a>
                </div>
              </div>
            </div>
          </div>

          <p>&nbsp;</p>

        </div>
      </div>
    </div>


  </section>

  <section class="section section--padding-around section--grey">
    <div class="container-fluid">

      <div class="row">
        <div class="col-bp1-12 col-bp2-6 col-bp5-4">

          <div class="post">
            <p class="post__date">7 September 2019</p>
            <img class="post__image" src="<?php echo site_url() . '/wp-content/uploads/2019/07/feature-placeholder1.jpg'; ?>" alt="post Image" />
            <h3 class="post__title">Latest news headline which can run up over a max of two lines</h3>
            <div class="post__desc">
              <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet.</p>
            </div>
            <a href="" class="button button--gold">Read more <span></span></a>
          </div>

        </div>
        <div class="col-bp1-12 col-bp2-6 col-bp5-4">

          <div class="post">
            <p class="post__date">7 September 2019</p>
            <img class="post__image" src="<?php echo site_url() . '/wp-content/uploads/2019/07/feature-placeholder1.jpg'; ?>" alt="post Image" />
            <h3 class="post__title">Latest news headline which can run up over a max of two lines</h3>
            <div class="post__desc">
              <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet.</p>
            </div>
            <a href="" class="button button--gold">Read more <span></span></a>
          </div>

        </div>
        <div class="col-bp1-12 col-bp2-6 col-bp5-4">

          <div class="post">
            <p class="post__date">7 September 2019</p>
            <img class="post__image" src="<?php echo site_url() . '/wp-content/uploads/2019/07/feature-placeholder1.jpg'; ?>" alt="post Image" />
            <h3 class="post__title">Latest news headline which can run up over a max of two lines</h3>
            <div class="post__desc">
              <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet.</p>
            </div>
            <a href="" class="button button--gold">Read more <span></span></a>
          </div>

        </div>
      </div>

    </div>
  </section>

  <p>&nbsp;</p>

  <section class="section">
    <div class="container-fluid">

      <div class="row">
        <div class="col-bp1-12">

          <div class="block block--blue block--text-light">
            <div class="block__content">
              <h4 class="block__title">Funding</h4>
              <p>Urban & Provincial are patient in sourcing new opportunities and are willing to spend significant time and resources in building conviction around an opportunity ensuring every approach is considered. This means our project teams will have a strong and comprehensive strategy for an opportunity before acquiring it.</p>
            </div>
            <div class="block__image">
              <img src="<?php echo site_url() . '/wp-content/uploads/2019/07/feature-placeholder1.jpg'; ?>" alt="" />
            </div>
          </div>

          <div class="block block--reverse block--white block--text-dark">
            <div class="block__content">
              <h4 class="block__title">Culture</h4>
              <p>Urban & Provincial has a “one team” culture with all professionals working together to originate and execute on the best opportunities for our strategy across the UK. Our team consists of experts and where we lack expertise we utilise a network of professional consultants to assist us in delivering our ideas.</p>
            </div>
            <div class="block__image">
              <img src="<?php echo site_url() . '/wp-content/uploads/2019/07/feature-placeholder1.jpg'; ?>" alt="" />
            </div>
          </div>

        </div>
      </div>

    </div>
  </section>

  <section class="section section--padding-around section--blue">
    <div class="container-fluid">

      <div class="row">
        <div class="col-bp1-12">

          <div class="carousel hide-carousel carousel-hide">
            <div class="js-carousel swiper-container">
              <div class="swiper-wrapper carousel-items carousel-items--space">
                <div class="swiper-slide">
                  <div class="consultant">
                    <img src="<?php echo site_url() . '/wp-content/uploads/2019/08/logo-placeholder.png'; ?>" alt="Logo" />
                  </div>
                </div>
                <div class="swiper-slide">
                  <div class="consultant">
                    <img src="<?php echo site_url() . '/wp-content/uploads/2019/08/logo-placeholder.png'; ?>" alt="Logo" />
                  </div>
                </div>
                <div class="swiper-slide">
                  <div class="consultant">
                    <img src="<?php echo site_url() . '/wp-content/uploads/2019/08/logo-placeholder.png'; ?>" alt="Logo" />
                  </div>
                </div>
                <div class="swiper-slide">
                  <div class="consultant">
                    <img src="<?php echo site_url() . '/wp-content/uploads/2019/08/logo-placeholder.png'; ?>" alt="Logo" />
                  </div>
                </div>
                <div class="swiper-slide">
                  <div class="consultant">
                    <img src="<?php echo site_url() . '/wp-content/uploads/2019/08/logo-placeholder.png'; ?>" alt="Logo" />
                  </div>
                </div>
                <div class="swiper-slide">
                  <div class="consultant">
                    <img src="<?php echo site_url() . '/wp-content/uploads/2019/08/logo-placeholder.png'; ?>" alt="Logo" />
                  </div>
                </div>
                <div class="swiper-slide">
                  <div class="consultant">
                    <img src="<?php echo site_url() . '/wp-content/uploads/2019/08/logo-placeholder.png'; ?>" alt="Logo" />
                  </div>
                </div>
                <div class="swiper-slide">
                  <div class="consultant">
                    <img src="<?php echo site_url() . '/wp-content/uploads/2019/08/logo-placeholder.png'; ?>" alt="Logo" />
                  </div>
                </div>
                <div class="swiper-slide">
                  <div class="consultant">
                    <img src="<?php echo site_url() . '/wp-content/uploads/2019/08/logo-placeholder.png'; ?>" alt="Logo" />
                  </div>
                </div>
              </div>

              <footer class="carousel__footer">
                <div class="carousel-nav">
                  <div class="carousel-nav-prev">
                    <i class="icon icon--arrow--left"></i>
                  </div>
                  <div class="carousel-nav-next">
                    <i class="icon icon--arrow--right"></i>
                  </div>
                </div>

                <div class="carousel-pager"></div>
              </footer>
            </div>
          </div>

        </div>
      </div>
    </div>
  </section>

  <p>&nbsp;</p>

</main>

<?php get_footer(); ?>