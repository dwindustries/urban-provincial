<?php

/*
 * Template Name: Capabilities
*/

get_header();
?>

<main class="main">

  <section class="section section--title">
    <div class="container-fluid">

      <h1 class="title title--gold title--overhang"><?php the_title() ?></h1>

      <div class="row">
        <div class="col-bp1-12">

          <?php get_template_part('elements/components/sections'); ?>

        </div>
      </div>

    </div>
  </section>

  <?php get_template_part('elements/components/cta-block'); ?>

</main>

<?php get_footer(); ?>