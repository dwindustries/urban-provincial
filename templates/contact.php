<?php

/*
 * Template Name: Contact
*/

get_header();

$contact_page_id = 23;
?>

<main class="main">

  <section class="section section--title">
    <div class="container-fluid">

      <h1 class="title title--gold title--overhang"><?php the_title() ?></h1>

      <div class="row">
        <div class="col-bp1-12">

          <div class="contact-block">
            <div class="contact-block__content">
              <?php if (have_posts()) : ?>
                <?php while (have_posts()) : ?>
                  <?php the_post(); ?>
                  <?php the_content(); ?>

                <?php endwhile ?>
              <?php endif ?>

              <h4>CALL</h4>
              <p><?php echo get_field('contact_telephone', $contact_page_id); ?></p>

              <h4>EMAIL</h4>
              <p><a href="mailto:<?php echo get_field('contact_email', $contact_page_id); ?>"><?php echo get_field('contact_email', $contact_page_id); ?></a></p>

              <h4>ADDRESS</h4>
              <p><?php echo get_field('contact_address', $contact_page_id); ?></p>

              <?php echo do_shortcode('[contact-form-7 id="119" title="Contact form 1"]') ?>
            </div>
            <div class="contact-block__map">
              <div id="js-map-2d" class="map map--contact" data-lat="51.51386" data-long="-0.0953357"></div>
            </div>
          </div>

        </div>
      </div>
    </div>

    <section class="section section--space">
      <div class="container-fluid">
        <div class="row">
          <div class="col-bp1-12">

            <?php get_template_part('elements/components/feature-bar'); ?>

          </div>
        </div>
      </div>
    </section>

</main>

<?php get_footer(); ?>