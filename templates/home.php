<?php

/*
 * Template Name: Home
*/

get_header();
?>

<main class="main">

  <?php get_template_part('elements/components/hero'); ?>

  <section class="section section--space">
    <div class="container-fluid">
      <div class="row">
        <div class="col-bp1-12">

          <div class="leaders">
            <div class="lead lead--white">
              <h2><?php echo get_field('homepage_intro_title'); ?></h2>
              <p><?php echo get_field('homepage_intro_desc'); ?></p>
              <a href="<?php echo get_field('homepage_intro_link_url'); ?>" class="button button--gold"><?php echo get_field('homepage_intro_link_text'); ?> <span></span></a>
            </div>

            <div class="lead lead--grey lead--triangle">
              <?php
              //Get Posts
              $posts_args = array(
                'post_type' => 'post',
                'showposts' => 1,
                'order' => 'DESC',
                'post_status' => 'publish',
              );
              $posts = get_posts($posts_args);
              ?>

              <?php if ($posts) : ?>
                <?php foreach ($posts as $post) : ?>

                  <div class="latest-post">
                    <h3 class="latest-post__title"><?php echo $post->post_title ?></h3>
                    <p class="latest-post__date"><?php echo get_the_date('F Y') ?></p>
                    <?php the_excerpt() ?>
                    <a href="<?php the_permalink() ?>" class="button button--gold">Read more <span></span></a>
                  </div>

                <?php endforeach ?>
              <?php endif ?>
            </div>
          </div>

        </div>
      </div>
    </div>
  </section>

  <section class="section section--space">
    <div class="container-fluid">
      <div class="row">
        <div class="col-bp1-12">

          <?php get_template_part('elements/components/feature-bar'); ?>

        </div>
      </div>
    </div>
  </section>

</main>

<?php get_footer(); ?>