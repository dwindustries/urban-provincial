<?php

/*
 * Template Name: About
*/

get_header();
?>

<main class="main">

  <section class="section section--title">
    <div class="container-fluid">

      <h1 class="title title--gold title--overhang"><?php the_title() ?></h1>

      <div class="row">
        <div class="col-bp1-12">

          <?php get_template_part('elements/components/sections'); ?>

        </div>
      </div>

    </div>
  </section>

  <div class="consultants">
    <div class="container-fluid">

      <h3 class="title title--gold title--consultants">Consultants</h3>

      <div class="row">
        <div class="col-bp1-12">

          <div class="carousel hide-carousel carousel-hide">
            <div class="js-carousel swiper-container">
              <div class="swiper-wrapper carousel-items carousel-items--space">
                <div class="swiper-slide">
                  <div class="consultant">
                    <img src="<?php echo site_url() . '/wp-content/uploads/2019/08/consultant-allsop.png'; ?>" alt="Allsop" />
                  </div>
                </div>
                <div class="swiper-slide">
                  <div class="consultant">
                    <img src="<?php echo site_url() . '/wp-content/uploads/2019/08/consultant-ibi.png'; ?>" alt="IBI" />
                  </div>
                </div>
                <div class="swiper-slide">
                  <div class="consultant">
                    <img src="<?php echo site_url() . '/wp-content/uploads/2019/08/consultant-hoare-lea.png'; ?>" alt="Hoare Lea" />
                  </div>
                </div>
                <div class="swiper-slide">
                  <div class="consultant">
                    <img src="<?php echo site_url() . '/wp-content/uploads/2019/08/consultant-waterman.png'; ?>" alt="Waterman" />
                  </div>
                </div>
                <div class="swiper-slide">
                  <div class="consultant">
                    <img src="<?php echo site_url() . '/wp-content/uploads/2019/08/consultant-rolfe-judd.png'; ?>" alt="Rolfe Judd" />
                  </div>
                </div>
                <div class="swiper-slide">
                  <div class="consultant">
                    <img src="<?php echo site_url() . '/wp-content/uploads/2019/08/consultant-farrells.png'; ?>" alt="Farrells" />
                  </div>
                </div>
                <div class="swiper-slide">
                  <div class="consultant">
                    <img src="<?php echo site_url() . '/wp-content/uploads/2019/08/consultant-paragon.png'; ?>" alt="Paragon" />
                  </div>
                </div>
                <div class="swiper-slide">
                  <div class="consultant">
                    <img src="<?php echo site_url() . '/wp-content/uploads/2019/08/consultant-investec.png'; ?>" alt="Investec" />
                  </div>
                </div>
                <div class="swiper-slide">
                  <div class="consultant">
                    <img src="<?php echo site_url() . '/wp-content/uploads/2019/08/consultant-lda-design.png'; ?>" alt="LDA Design" />
                  </div>
                </div>
              </div>

              <footer class="carousel__footer">
                <div class="carousel-nav">
                  <div class="carousel-nav-prev">
                    <i class="icon icon--arrow--left"></i>
                  </div>
                  <div class="carousel-nav-next">
                    <i class="icon icon--arrow--right"></i>
                  </div>
                </div>

                <div class="carousel-pager"></div>
              </footer>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>

  <?php get_template_part('elements/components/cta-block'); ?>

</main>

<?php get_footer(); ?>