<?php

/**
 * The default template for a page.
 *
 * @package     WordPress
 * @subpackage  Urban Provincial
 * @since       Urban Provincial 1.0
 */

get_header();
?>

<main class="main">

  <section class="section section--padding-around">
    <div class="container-fluid">
      <div class="row center-bp1">
        <div class="col-bp1-12 col-bp5-7">
          <?php if (have_posts()) : ?>
          <?php while (have_posts()) : ?>
          <?php the_post(); ?>
          <h1 class="title title--gold"><?php the_title() ?></h1>

          <article class="single-post__article">
            <?php the_content(); ?>
          </article>

          <?php endwhile ?>
          <?php endif ?>
        </div>
      </div>
    </div>
  </section>

  <?php get_footer(); ?>