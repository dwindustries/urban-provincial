function AjaxPagination() {
	(function($) {
		// The number of the next page to load (/page/x/).
		let pageNum = parseInt(ajaxpagination.startPage) + 1;

		// The maximum number of pages the current query can return.
		let max = parseInt(ajaxpagination.maxPages);

		// The link of the next page of posts.
		let nextLink = ajaxpagination.nextLink;

		// Button selector
		const trigger = ".js-pagination-trigger";

		/**
		 * Replace the traditional navigation with our own,
		 * but only if there is at least one page of new posts to load.
		 */
		if (pageNum <= max) {
			// Insert the "Load More Posts" link.
			$(".posts")
				.append(
					'<div class="posts-placeholder posts-placeholder-' +
						pageNum +
						'"></div>'
				)
				.append(
					'<a class="js-pagination-trigger load-more" href="#"><span>Load More</span></a>'
				);

			// Remove the traditional navigation.
			$(".js-pagination").remove();
		}

		/**
		 * Load new posts when the link is clicked.
		 */
		$(trigger).click(function() {
			// Are there more posts to load?
			if (pageNum <= max) {
				// Show that we're working.
				$(this).html("<span>Loading...</span>");

				$(".posts-placeholder-" + pageNum).load(
					nextLink + " .post",
					function() {
						// Update page number and nextLink.
						pageNum++;
						nextLink = nextLink.replace(/\/page\/[0-9]?/, "/page/" + pageNum);

						// Add a new placeholder, for when user clicks again.
						$(trigger).before(
							'<div class="posts-placeholder posts-placeholder-' +
								pageNum +
								'"></div>'
						);

						// Update the button message.
						if (pageNum <= max) {
							$(trigger).html("<span>Load More</span>");
						} else {
							$(trigger).hide();
						}
					}
				);
			} else {
				$(trigger).append(".");
			}

			return false;
		});
	})(jQuery);
}

export default AjaxPagination;
