import Swiper from "swiper";

const CarouselSingle = () => {
	var carouselSingle = new Swiper(".js-carousel-single", {
		slidesPerView: 1,
		loop: true,
		speed: 1000,
		effect: "fade",
		autoplay: {
			delay: 4000
		},
		navigation: {
			nextEl: ".carousel-nav-next",
			prevEl: ".carousel-nav-prev"
		},
		pagination: {
			el: ".carousel-pager",
			clickable: true
		},
		on: {
			init: function() {
				const revealEls = document.querySelectorAll(".hide-carousel");
				for (let i = 0; i < revealEls.length; i++) {
					revealEls[i].classList.add("reveal-carousel");
				}
			}
		}
	});
};

export default CarouselSingle;
