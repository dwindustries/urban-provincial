import loadGoogleMapsApi from "load-google-maps-api";
import { mapStyle } from "../libs/utils";

function Map(mapRelief) {
	function init() {
		loadGoogleMapsApi({ key: "AIzaSyDNKg_UZQgiCORhmX6114I5iW4VNgfYsaw" })
			.then(function(googleMaps) {
				window.onload = initialize(googleMaps);
			})
			.catch(function(error) {
				console.log(error);
			});
	}

	const initialize = googleMaps => {
		window.marker = null;

		const mapEl = document.getElementById(`js-map-${mapRelief}`);
		const position = new googleMaps.LatLng(
			getLatitude(mapEl),
			getLongitude(mapEl)
		);

		let mapOptions = {
			// SET THE CENTER
			center: position,
			// SET THE BACKGROUND COLOUR
			backgroundColor: "#aaaaaa",
			// REMOVE ALL THE CONTROLS EXCEPT ZOOM
			panControl: false,
			zoomControl: false,
			mapTypeControl: false,
			scaleControl: false,
			streetViewControl: false,
			overviewMapControl: false,
			zoomControlOptions: false
		};

		if (mapRelief == "2d") {
			mapOptions.mapTypeId = "roadmap";
			mapOptions.zoom = 15;
		} else {
			mapOptions.mapTypeId = "satellite";
			mapOptions.zoom = 18;
			mapOptions.rotateControl = true;
			mapOptions.tilt = 45;
		}

		const map = new googleMaps.Map(mapEl, mapOptions);

		let pointerSize;
		if (mapRelief == "2d") {
			const style = mapStyle;
			const mapType = new googleMaps.StyledMapType(style, {
				name: "Grayscale"
			});
			map.mapTypes.set("grey", mapType);
			map.setMapTypeId("grey");
			pointerSize = new googleMaps.Size(96, 123);
		} else {
			map.setTilt(45);
			pointerSize = new googleMaps.Size(67, 86);
		}

		const icon = new googleMaps.MarkerImage(
			"/wp-content/themes/urbanprovincial/assets/build/images/svgs/map-pointer.svg",
			null,
			null,
			null,
			pointerSize
		);

		marker = new googleMaps.Marker({
			position,
			map,
			icon
		});
	};

	const getLatitude = map => {
		return map.getAttribute("data-lat");
	};

	const getLongitude = map => {
		return map.getAttribute("data-long");
	};

	return { init };
}

export default Map;
