function NavToggle() {
	var DOM = {};

	// cache DOM elements
	function cacheDOM() {
		DOM = {
			trigger: document.querySelector(".js-nav-trigger"),
			nav: document.getElementById("js-nav")
		};
	}

	// Bind the click event to trigger
	function bindEvents() {
		DOM.trigger.addEventListener(
			"click",
			event => {
				event.preventDefault();
				handleMenuTrigger();
			},
			false
		);
	}

	// Handle the trigger click (just render class toggling)
	function handleMenuTrigger() {
		render();
	}

	// Render our DOM manipulation
	function render() {
		DOM.trigger.classList.toggle("active");
		DOM.nav.classList.toggle("active");
	}

	// Init method to call
	function init() {
		cacheDOM();
		bindEvents();
	}

	// Return our init method to kick us off
	return { init };
}

export default new NavToggle();
