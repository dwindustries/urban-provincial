import Swiper from "swiper";

const Carousel = () => {
	var c = new Swiper(".js-carousel", {
		slidesPerView: 5,
		spaceBetween: 0,
		loop: true,
		keyboard: true,
		navigation: {
			nextEl: ".carousel-nav-next",
			prevEl: ".carousel-nav-prev"
		},
		autoplay: {
			delay: 2000
		},
		pagination: {
			el: ".carousel-pager"
		},
		breakpoints: {
			400: {
				slidesPerView: 1
			},
			700: {
				slidesPerView: 2
			},
			1000: {
				slidesPerView: 3
			}
		},
		on: {
			init: function() {
				let revealEls = document.querySelectorAll(".hide-carousel");
				for (let i = 0; i < revealEls.length; i++) {
					revealEls[i].classList.add("reveal-carousel");
				}
			}
		}
	});
};

export default Carousel;
