import { closest } from "../libs/utils";

const OverlayToggle = () => {
	var DOM = {};

	// cache DOM elements
	function cacheDOM() {
		DOM = {
			triggers: document.querySelectorAll(".js-overlay-btn"),
			elementClass: "js-overlay"
		};
	}

	// Bind the click event to trigger
	function bindEvents() {
		let _triggers = DOM.triggers;
		for (let i = 0; i < _triggers.length; i++) {
			_triggers[i].addEventListener(
				"click",
				event => {
					event.preventDefault();
					handleOverlayTrigger(event.currentTarget);
				},
				true
			);
		}
	}

	// Handle the trigger click (just render class toggling)
	function handleOverlayTrigger(btn) {
		// Find our overlay container by element selector
		var overlay = closest(btn, el => {
			return el.classList.contains(DOM.elementClass);
		});

		render({ btn, overlay });
	}

	// Render our DOM manipulation
	function render(props = {}) {
		// Add active class to clicked trigger
		props.btn.classList.toggle("active");

		// Add active class to container
		props.overlay.classList.toggle("active");
	}

	// Init method to call
	function init() {
		cacheDOM();
		bindEvents();
	}

	// Return our init method to kick us off
	return { init };
};

export default new OverlayToggle();
