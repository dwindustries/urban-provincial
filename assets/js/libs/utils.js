// Get closest element
const closest = function(el, fn) {
	return el && (fn(el) ? el : closest(el.parentNode, fn));
};

// Map Styles
const mapStyle = [
	{
		elementType: "geometry",
		stylers: [
			{
				color: "#dddddd"
			}
		]
	},
	{
		elementType: "labels.icon",
		stylers: [
			{
				visibility: "off"
			}
		]
	},
	{
		elementType: "labels.text.fill",
		stylers: [
			{
				color: "#616161"
			}
		]
	},
	{
		elementType: "labels.text.stroke",
		stylers: [
			{
				color: "#f5f5f5"
			}
		]
	},
	{
		featureType: "administrative.land_parcel",
		elementType: "labels",
		stylers: [
			{
				visibility: "off"
			}
		]
	},
	{
		featureType: "administrative.land_parcel",
		elementType: "labels.text.fill",
		stylers: [
			{
				color: "#bdbdbd"
			}
		]
	},
	{
		featureType: "poi",
		elementType: "geometry",
		stylers: [
			{
				color: "#eeeeee"
			}
		]
	},
	{
		featureType: "poi",
		elementType: "labels.text",
		stylers: [
			{
				visibility: "off"
			}
		]
	},
	{
		featureType: "poi",
		elementType: "labels.text.fill",
		stylers: [
			{
				color: "#757575"
			}
		]
	},
	{
		featureType: "poi.business",
		stylers: [
			{
				visibility: "off"
			}
		]
	},
	{
		featureType: "poi.park",
		elementType: "geometry",
		stylers: [
			{
				color: "#e5e5e5"
			}
		]
	},
	{
		featureType: "poi.park",
		elementType: "labels.text",
		stylers: [
			{
				visibility: "off"
			}
		]
	},
	{
		featureType: "poi.park",
		elementType: "labels.text.fill",
		stylers: [
			{
				color: "#9e9e9e"
			}
		]
	},
	{
		featureType: "road",
		elementType: "geometry",
		stylers: [
			{
				color: "#cccccc"
			}
		]
	},
	{
		featureType: "road.arterial",
		elementType: "labels",
		stylers: [
			{
				visibility: "off"
			}
		]
	},
	{
		featureType: "road.arterial",
		elementType: "labels.text.fill",
		stylers: [
			{
				visibility: "off"
			}
		]
	},
	{
		featureType: "road.highway",
		elementType: "geometry",
		stylers: [
			{
				color: "#cccccc"
			}
		]
	},
	{
		featureType: "road.highway",
		elementType: "labels",
		stylers: [
			{
				visibility: "off"
			}
		]
	},
	{
		featureType: "road.highway",
		elementType: "labels.text.fill",
		stylers: [
			{
				color: "#616161"
			}
		]
	},
	{
		featureType: "road.local",
		stylers: [
			{
				visibility: "on"
			}
		]
	},
	{
		featureType: "road.local",
		elementType: "labels",
		stylers: [
			{
				visibility: "off"
			}
		]
	},
	{
		featureType: "road.local",
		elementType: "labels.text.fill",
		stylers: [
			{
				color: "#9e9e9e"
			}
		]
	},
	{
		featureType: "transit.line",
		elementType: "geometry",
		stylers: [
			{
				visibility: "off"
			}
		]
	},
	{
		featureType: "transit.station",
		elementType: "geometry",
		stylers: [
			{
				color: "#cccccc"
			}
		]
	},
	{
		featureType: "water",
		elementType: "geometry",
		stylers: [
			{
				color: "#fefefe"
			}
		]
	},
	{
		featureType: "water",
		elementType: "labels.text.fill",
		stylers: [
			{
				visibility: "off"
			}
		]
	}
];

export { closest, mapStyle };
