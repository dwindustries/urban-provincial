"use strict";

import NavToggle from "./components/NavToggle";
import Carousel from "./components/Carousel";
import OverlayToggle from "./components/OverlayToggle";
import CarouselSingle from "./components/CarouselSingle";
import Map from "./components/Map";
import AjaxPagination from "./components/AjaxPagination";

document.addEventListener(
	"DOMContentLoaded",
	event => {
		NavToggle.init();
		Carousel();
		CarouselSingle();
		OverlayToggle.init();

		// Init all our maps
		const map1 = new Map("2d");
		const map2 = new Map("3d");
		map1.init();
		map2.init();

		AjaxPagination();
	},
	true
);
