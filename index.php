<?php

/**
 * The default template file.
 *
 * @package     WordPress
 * @subpackage  Urban Provincial
 * @since       Urban Provincial 1.0
 */

get_header();
?>


<main class="main">

  <section class="section section--news">
    <div class="container-fluid">

      <h1 class="title title--gold title--news"><?php echo get_the_title(get_option('page_for_posts', true)) ?></h1>

      <?php if (have_posts()) : ?>
      <div class="posts">
        <?php while (have_posts()) : ?>
        <?php the_post(); ?>

        <div class="post">
          <p class="post__date"><?php echo get_the_date('F Y') ?></p>
          <a href="<?php the_permalink() ?>"><?php the_post_thumbnail('article-list', array('class' => 'post__image')) ?></a>
          <h3 class="post__title"><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h3>
          <div class="post__desc">
            <?php the_excerpt() ?>
          </div>
          <a href="<?php the_permalink() ?>" class="button button--gold">Read more <span></span></a>
        </div>

        <?php endwhile ?>
      </div>
      <?php endif ?>

      <a class="js-pagination button">Load Older Articles</a>

    </div>
  </section>

  <section class="section section--space">
    <div class="container-fluid">
      <div class="row">
        <div class="col-bp1-12">

          <?php get_template_part('elements/components/feature-bar'); ?>

        </div>
      </div>
    </div>
  </section>

  <?php get_template_part('elements/components/cta-block'); ?>

</main>

<?php get_footer(); ?>