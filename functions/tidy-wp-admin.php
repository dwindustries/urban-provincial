<?php
/*
 * 
 * Functions to tidy up the WP Admin Screens
 * 
 */

// Remove the Wordpress update notification
add_action('admin_menu', 'wphidenag');
function wphidenag()
{
  remove_action('admin_notices', 'update_nag', 3);
}

// Tidy up the header
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'wp_generator');

// Remove unwanted admin items
function remove_menus()
{

  //remove_menu_page( 'themes.php' );                 //Appearance      
  remove_menu_page('edit-comments.php');          //Comments
  // remove_menu_page( 'plugins.php' );                //Plugins
  // remove_menu_page('tools.php');                  //Tools
  // remove_menu_page( 'options-general.php' );        //Settings
  //remove_menu_page( 'upload.php' );               //Media
  //remove_menu_page( 'wpcf7' );                      //Contact

  remove_submenu_page('index.php', 'update-core.php'); // Remove Updates menu item
  remove_submenu_page('themes.php', 'themes.php');
  global $submenu;
  // unset($submenu['themes.php'][6]); // Customize link

}
add_action('admin_menu', 'remove_menus');


// Remove theme editor
function remove_theme_editor()
{

  remove_submenu_page('themes.php', 'theme-editor.php');
}
add_action('admin_init', 'remove_theme_editor');

// Modify Admin Toolbar items
add_action('admin_bar_menu', 'modify_toolbar_items', 999);
function modify_toolbar_items($wp_admin_bar)
{
  // Remove items
  $wp_admin_bar->remove_node('wp-logo');
  $wp_admin_bar->remove_node('comments');

  // Modify Howdy
  $my_account = $wp_admin_bar->get_node('my-account');
  $newtitle   = str_replace('Howdy,', 'Hi', $my_account->title);
  $wp_admin_bar->add_node(array(
    'id' => 'my-account',
    'title' => $newtitle,
  ));
}


// Remove wp emoji code on frontend
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');
remove_action('admin_print_scripts', 'print_emoji_detection_script');
remove_action('admin_print_styles', 'print_emoji_styles');


function title_format($content)
{
  return '%s';
}
add_filter('private_title_format', 'title_format');
add_filter('protected_title_format', 'title_format');


// Remove Default Image Link
function remove_image_link()
{
  $image_set = get_option('image_default_link_type');

  if ($image_set !== 'none') {
    update_option('image_default_link_type', 'none');
  }
}
add_action('admin_init', 'remove_image_link', 10);
