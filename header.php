<?php

/**
 * The Header for our theme
 *
 * Displays the <head> and <header> sections
 *
 * @package WordPress
 * @subpackage Urban Provincial
 * @since Urban Provincial 1.0
 */
?>

<!doctype html>
<html class="no-js" lang="">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Urban Provincial</title>
<meta name="description" content="">
<meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1, maximum-scale=1">

<link href="https://fonts.googleapis.com/css?family=Barlow:400,400i,700,700i&display=swap" rel="stylesheet">
<link rel="icon" href="<?php echo get_stylesheet_directory_uri() . '/assets/images/icons/favicon.ico'; ?>" type="image/x-icon" />

<script type="text/javascript">
  // Apply js class to html
  document.documentElement.className = 'js';
</script>
<style type="text/css">
  /*Hide nav on page load if js available*/
  /* .js .site-header { display:none; }  */
</style>

<!--[if lt IE 9]>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script>window.html5 || document.write('<script src="js/vendor/html5shiv.js"><\/script>')</script>
	<![endif]-->

<!-- Global site tag (gtag.js) - Google Analytics -->

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-146276456-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];

  function gtag() {
    dataLayer.push(arguments);
  }
  gtag('js', new Date());
  gtag('config', 'UA-146276456-1');
</script>

<?php wp_head(); ?>
</head>

<body>

  <div id="top" class="wrapper">
    <div class="wrapper__inner">
      <header id="site-header" class="site-header">
        <div class="site-header__inner">
          <a href="/" class="site-logo">
            <img src="<?php echo get_template_directory_uri() . '/assets/build/images/logos/urban-and-provincial-logo.svg'; ?>" alt="Urban &amp; Provincial" />
          </a>

          <?php $contact_page_id = 23; ?>

          <div class="site-header__toolbar">
            <ul class="social__items">
              <li class="social__item">
                <a href="https://twitter.com/<?php echo get_field('social_twitter', $contact_page_id); ?>" target="_blank">
                  <i class="icon icon--social icon--social-twitter"></i>
                </a>
              </li>
              <li class="social__item">
                <a href="https://www.linkedin.com/company/<?php echo get_field('social_linkedin', $contact_page_id); ?>" target="_blank">
                  <i class="icon icon--social icon--social-linkedin"></i>
                </a>
              </li>
              <li class="social__item">
                <a href="https://www.instagram.com/<?php echo get_field('social_instagram', $contact_page_id); ?>" target="_blank">
                  <i class="icon icon--social icon--social-instagram"></i>
                </a>
              </li>
            </ul>

            <a class="js-nav-trigger site-nav-trigger" title="View Menu">
              <span class="lines-button x">
                <span class="lines"></span>
              </span>
            </a>
          </div>
        </div>


        <div id="js-nav" class="main-navigation">
          <?php
          wp_nav_menu(
            array(
              'theme_location'   => 'primary-menu',
              'container_class'   => 'nav-primary-container',
              'menu_class'     => 'js-nav-items nav-primary',
              'depth' => 1
            )
          );
          ?>
        </div>
      </header>