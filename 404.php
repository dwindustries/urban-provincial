<?php

/**
 * The 404 page template
 *
 * @package     WordPress
 * @subpackage  Urban Provincial
 * @since       Urban Provincial 1.0
 */

get_header();
?>

<section class="section">
  <div class="container-fluid">

    <div class="row">
      <div class="col-bp1-12 col-bp2-9">
        <h1>404</h1>
        <h2>Page Not Found</h2>
        <p>Sorry we couldn't find the page you requested, please try again or select an item from the navigation.</p>
      </div>
    </div>

  </div>
</section>

<?php get_footer(); ?>